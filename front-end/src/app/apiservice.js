import axios from "axios";

const httpClient = axios.create({
    baseURL: 'http://localhost:8080'
})

class ApiService {

    constructor(apiUrl){
        this.apiUrl = apiUrl;
    }

    obeterListaMeses(){
        return [
            {label: 'Selecione...', value: ''},
            {label: 'Janeiro', value: '1'},
            {label: 'Fevereiro', value: '2'},
            {label: 'Março', value: '3'},
            {label: 'Abril', value: '4'},
            {label: 'Maio', value: '5'},
            {label: 'Junho', value: '6'},
            {label: 'Julho', value: '7'},
            {label: 'Agosto', value: '8'},
            {label: 'Setembro', value: '9'},
            {label: 'Outubro', value: '10'},
            {label: 'Novembro', value: '11'},
            {label: 'Dezembro', value: '12'}
        ]
    }

    obterListaTipos(){
        return   [
            {label: 'Selecione...', value: ''},
            {label: 'RECEITA', value: '1'},
            {label: 'DESPESA', value: '2'}
        ]
    }

    post(url, obj){
        const requestUrl = `${this.apiUrl}${url}`
        return httpClient.post(requestUrl, obj);
    }
    put(url, obj){
        const requestUrl = `${this.apiUrl}${url}`
        return httpClient.put(requestUrl, obj);
    }

    delete(url){
        const requestUrl = `${this.apiUrl}${url}`
        return httpClient.delete(requestUrl);
    }

    get(url){
        const requestUrl = `${this.apiUrl}${url}`
        return httpClient.get(requestUrl);
    }
}

export default ApiService