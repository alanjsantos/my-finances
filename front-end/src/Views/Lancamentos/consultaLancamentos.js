import React from "react";
import { withRouter } from "react-router-dom";
import Card from "../../Components/card";
import FormGroup from "../../Components/form-group";
import SelectMenu from "../../Components/selectMenu";
import LancamentosTable from './lancamentosTable';
import LancamentoService from './../../app/service/lancamentoService';
import LocalStorageService from "../../app/service/localStorageService";
import * as messages from '../../Components/toastr'
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';


class ConsultaLancamentos extends React.Component {


  state = {
    ano: '',
    mes: '',
    tipo: '',
    descricao: '',
    showConfirmDialog: false,
    lancamentoDeletar: {},
    lancamentos: []
  }

  constructor() {
    super();
    this.service = new LancamentoService();
  }

  buscar = () => {

    if (!this.state.ano) {
      messages.mensagemErro('Campo ano é obrigatório.')

      return false;
    }

    const usuarioLogado = LocalStorageService.obterItem('_usuario_logado')

    const lancamentoFiltro = {
      ano: this.state.ano,
      mes: this.state.mes,
      tipo: this.state.tipo,
      descricao: this.state.descricao,
      usuario: usuarioLogado.id
    }

    this.service
      .consultar(lancamentoFiltro)
      .then((response) => {
        this.setState({ lancamentos: response.data })
      }).catch((error) => {
        console.log(error.response.data);
      })


    console.log(this.state)
  }

  editar = (id) => {
    console.log('Editando Lancamento', id);
  }

  abrirConfirmacao = (lancamento) => {
    this.setState({showConfirmDialog: true, lancamentoDeletar: lancamento})
  }

  cancelarDelecao  = () => {
    this.setState({showConfirmDialog: false, lancamentoDeletar: {}})
  }

  

  deletar = () => {

    this.service
      .deletar(this.state.lancamentoDeletar.id)
      .then(resposne => {

        //removendo o lancamneto do Array para atualizar a tela após realizar o delete.
        const lancamentos = this.state.lancamentos;
        const index = lancamentos.indexOf(this.state.lancamentoDeletar)

        lancamentos.splice(index, 1);
        this.setState({lancamentos: lancamentos, showConfirmDialog: false});
        messages.mensagemSucesso('Lancamento deletado com sucesso.')


      }).catch(error => {
        messages.mensagemErro('Erro ao tentar deletar o Lancamento.')
      })
    //console.log('Deletando lancamento', id);
  }



  render() {

    const listaMes = this.service.obeterListaMeses();
    const tipoLancamento = this.service.obterListaTipos();

    //Botão de confimar ou cancelar lancamento.
    const ConfirmDialog = () => {
      return (
          <div>
              <Button label="Confirmar" icon="pi pi-check" onClick={this.deletar} autoFocus />
              <Button label="Cancelar" icon="pi pi-times" onClick={this.cancelarDelecao} className="p-button-text" />
          </div>
      );
  }

    return (
      <div className="container">
        <Card title="Consulta Lancamentos">
          <div className="row">
            <div className="col-lg-6">
              <div className="bs-component">
                

                <FormGroup label="Descrição" htmlFor="inputDescricao">
                  <input
                    type="text"
                    class="form-control"
                    id="inputDescricao"
                    placeholder="Digite a Descrição"
                    value={this.state.descricao}
                    onChange={(e) => this.setState({ descricao: e.target.value })}
                    
                  />
                </FormGroup>
                <FormGroup label="Ano" htmlFor="inputAno">
                  <input
                    type="text"
                    class="form-control"
                    id="inputAno"
                    placeholder="Digite o Ano"
                    value={this.state.ano}
                    onChange={(e) => this.setState({ ano: e.target.value })}
                  />
                </FormGroup>


                <FormGroup label="Mes" >
                  <SelectMenu lista={listaMes} className="inputMes"
                    type="text"
                    class="form-control"
                    id="inputMes"
                    placeholder="Digite o Mes"
                    value={this.state.mes}
                    onChange={(e) => this.setState({ mes: e.target.value })} />
                </FormGroup>

                <FormGroup label="Tipo Lancamento" >
                  <SelectMenu lista={tipoLancamento}
                    className="form-control"
                    id="inputTipoLancamento"
                    value={this.state.tipo}
                    onChange={(e) => this.setState({ tipo: e.target.value })} />
                </FormGroup>

                <button type="button" class="btn btn-success" onClick={this.buscar}>Buscar</button>
                <button type="button" class="btn btn-danger">Cadastrar</button>

              </div>
            </div>
          </div>

        </Card>
        <div className="row">
          <div className="col-md-12">
            <div className="bs-component">
              <LancamentosTable lancamentos={this.state.lancamentos}
                editAction={this.editar}
                deleteAction={this.abrirConfirmacao} />
            </div>
          </div>
        </div>
        <div>
          <Dialog 
          header="Header" 
          visible={this.state.showConfirmDialog} 
          footer={ConfirmDialog}
          style={{ width: '50vw' }} 
          onHide={() => this.setState({showConfirmDialog: false})}>
            <p>Confirmar a exclusão deste Lançamento?</p>
          </Dialog>
        </div>




      </div>
    );
  }
}

export default withRouter(ConsultaLancamentos);
